package nur.reza.appx0bandroid

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        btnMhs.setOnClickListener(this)
        btnProdi.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMhs->{
                val intent = Intent(this,MhsActivity::class.java)
                startActivity(intent)
            }
            R.id.btnProdi->{
                val intent = Intent(this,ProdiActivity::class.java)
                startActivity(intent)
            }
        }
    }

}
