package nur.reza.appx0bandroid

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.toolbox.StringRequest
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_prodi.*
import org.json.JSONArray
import org.json.JSONObject

class ProdiActivity : AppCompatActivity(), View.OnClickListener {
    val activity = this@ProdiActivity
    lateinit var proAdapter : AdapterDataProdi
    var daftarPro = mutableListOf<HashMap<String,String>>()
    val url = "http://localhost/web/Android/Appx0b/show_prodi.php"
    val url2 = "http://localhost/web/Android/Appx0b/pro_upd_del_ins.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        proAdapter = AdapterDataProdi(daftarPro,this)
        setContentView(R.layout.activity_prodi)
        lsProdi.layoutManager = LinearLayoutManager(this)
        lsProdi.adapter = proAdapter
        btnUpdPr.setOnClickListener(this)
        btnDelPr.setOnClickListener(this)
        btnInsPr.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnUpdPr->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnInsPr->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnDelPr->{
                queryInsertUpdateDelete("delete")
            }
        }
    }
    override fun onStart() {
        super.onStart()
        showDataPro()
    }

    fun showDataPro(){
        val request =object : StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarPro.clear()
                Toast.makeText(this,"koneksi ke server (data mhs)",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("id_prodi",jsonObject.getString("id_prodi"))
                    mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    daftarPro.add(mhs)
                }
                proAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            }){
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(Method.POST,url2,
            Response.Listener { response ->
                Toast.makeText(this,"Berubah data",Toast.LENGTH_SHORT).show()
                val jsonObject = JSONObject(response)
                val error= jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_SHORT).show()
                    showDataPro()
                }
                else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung dengan server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("id_prodi",idProdi.text.toString())
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}
