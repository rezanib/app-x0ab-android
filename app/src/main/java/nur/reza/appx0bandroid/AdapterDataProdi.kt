package nur.reza.appx0bandroid

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_prodi.*


class AdapterDataProdi(val dataProdi : List<HashMap<String,String>>,val proActivity : ProdiActivity) : RecyclerView.Adapter<AdapterDataProdi.HolderDataProdi>(){
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): AdapterDataProdi.HolderDataProdi {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.row_prodi,parent,false)
            return HolderDataProdi(v)
        }

        override fun getItemCount(): Int {
            return dataProdi.size
        }
//
        override fun onBindViewHolder(holder: AdapterDataProdi.HolderDataProdi, position: Int) {
            val data = dataProdi.get(position)
            holder.txIdProdi.setText(data.get("id_prodi"))
            holder.txNamaProdi.setText(data.get("nama_prodi"))
            if(position.rem(2)==0){
                holder.CLayout.setBackgroundColor(Color.rgb(230,245,240))
            }
            else{
                holder.CLayout.setBackgroundColor(Color.rgb(255,255,245))
            }
            holder.CLayout.setOnClickListener(View.OnClickListener {
                proActivity.edNamaProdi.setText(data.get("nama_prodi"))
            })
        }

        inner class HolderDataProdi(v: View): RecyclerView.ViewHolder(v){
            val txIdProdi = v.findViewById<TextView>(R.id.txIdProdi)
            val txNamaProdi = v.findViewById<TextView>(R.id.txNamaProdi)
            val CLayout = v.findViewById<ConstraintLayout>(R.id.CLayout)
        }
    }

